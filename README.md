# OSI² KiCAD Library

KiCAD libaries for the development of PCBs within the OSI² community

Please make sure that you refer to a specific version of this library when developing PCBs (e.g. in the corresponding REAMDE file).

## License

This library is licensed under the GNU Lesser General Public License Version 3.0 (LGPLv3), see [LICENSE](LICENSE.md) for details.

Consequently, you're free to use the library for developments under any license (even proprietary developments), but modifications (/derivatives) of the library can only be distributed under LGPLv3 (or GPLv3).
